package pt.up.fe.tiago.cafeteriaterminal.initializer;

import android.app.Application;

import pt.up.fe.tiago.cafeteriaterminal.dependencyinjection.ApplicationComponent;
import pt.up.fe.tiago.cafeteriaterminal.dependencyinjection.DaggerApplicationComponent;
import pt.up.fe.tiago.cafeteriaterminal.dependencyinjection.NetworksModule;
import pt.up.fe.tiago.cafeteriaterminal.utils.Constants;

public class CafeteriaTerminalApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                                .networksModule(new NetworksModule(Constants.URL))
                                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }
}
