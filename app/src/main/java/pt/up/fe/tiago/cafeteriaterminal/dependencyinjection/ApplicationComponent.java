package pt.up.fe.tiago.cafeteriaterminal.dependencyinjection;

import javax.inject.Singleton;

import dagger.Component;
import pt.up.fe.tiago.cafeteriaterminal.MainActivity;

@Singleton
@Component(modules = {NetworksModule.class})
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
}
