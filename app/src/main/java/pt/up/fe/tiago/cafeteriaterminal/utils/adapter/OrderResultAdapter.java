package pt.up.fe.tiago.cafeteriaterminal.utils.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.cafeteriaterminal.R;
import pt.up.fe.tiago.cafeteriaterminal.domain.Product;

public class OrderResultAdapter extends RecyclerView.Adapter<OrderResultAdapter.OrderResultHolder> {
    private List<Product> products;
    private List<Integer> numberOfProducts;

    static class OrderResultHolder extends RecyclerView.ViewHolder {
        TextView productName;
        TextView productPrice;
        TextView numberOfProducts;
        TextView totalPrice;

        OrderResultHolder(View v) {
            super(v);
            productName = v.findViewById(R.id.product_name);
            productPrice = v.findViewById(R.id.product_price);
            numberOfProducts = v.findViewById(R.id.product_number);
            totalPrice = v.findViewById(R.id.product_total);
        }
    }

    public OrderResultAdapter(List<Product> products, List<Integer> numberOfProducts) {
        this.products = products;
        this.numberOfProducts = numberOfProducts;
    }

    @NonNull
    @Override
    public OrderResultAdapter.OrderResultHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_card_item, parent, false);
        return new OrderResultHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderResultHolder holder, int position) {

        holder.productName.setText(products.get(position).getName());
        holder.productPrice.setText(String.valueOf(products.get(position).getPrice() + "$"));
        holder.numberOfProducts.setText(String.valueOf(numberOfProducts.get(position)));
        holder.totalPrice.setText(String.valueOf(products.get(position).getPrice() * numberOfProducts.get(position) + "$"));
    }

    public void addOrderInformation(List<Product> products, List<Integer> numberOfProducts) {
        for(int i = 0; i < products.size(); i++) {
            this.products.add(products.get(i));
            this.numberOfProducts.add(numberOfProducts.get(i));
            notifyItemInserted(this.products.indexOf(products.get(i)));
        }
    }

    public void removeData() {
        this.products.clear();
        this.numberOfProducts.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
