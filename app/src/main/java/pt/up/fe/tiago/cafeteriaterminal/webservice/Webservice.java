package pt.up.fe.tiago.cafeteriaterminal.webservice;

import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CafeteriaOrderResultDTO;
import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CreateOrderDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Webservice {

    @POST("invoice/process")
    Call<CafeteriaOrderResultDTO> processOrderInvoice(@Body CreateOrderDTO createOrderDTO);

}