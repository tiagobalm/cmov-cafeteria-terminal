package pt.up.fe.tiago.cafeteriaterminal.webservice.dto;

import java.io.Serializable;
import java.util.List;

import pt.up.fe.tiago.cafeteriaterminal.domain.Product;
import pt.up.fe.tiago.cafeteriaterminal.domain.Voucher;

public class CafeteriaOrderResultDTO implements Serializable {
    private String result;
    private Integer invoiceID;
    private List<Product> products;
    private List<Integer> numberOfProducts;
    private Double totalPrice;
    List<Voucher> vouchers;

    public CafeteriaOrderResultDTO() {}

    public CafeteriaOrderResultDTO(String result, Integer invoiceID, List<Product> products, List<Integer> numberOfProducts, Double totalPrice, List<Voucher> vouchers) {
        this.result = result;
        this.invoiceID = invoiceID;
        this.products = products;
        this.numberOfProducts = numberOfProducts;
        this.totalPrice = totalPrice;
        this.vouchers = vouchers;
        this.totalPrice = totalPrice;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Integer invoiceID) {
        this.invoiceID = invoiceID;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Integer> getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(List<Integer> numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
}
