package pt.up.fe.tiago.cafeteriaterminal.webservice.dto;

import java.io.Serializable;

public class CreateOrderDTO implements Serializable {

    private byte[] signedData;
    private byte[] data;

    public CreateOrderDTO() {}

    public CreateOrderDTO(byte[] signedData, byte[] data) {
        this.signedData = signedData;
        this.data = data;
    }

    public byte[] getSignedData() {
        return signedData;
    }

    public void setSignedData(byte[] signedData) {
        this.signedData = signedData;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
