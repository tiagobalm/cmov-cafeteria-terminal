package pt.up.fe.tiago.cafeteriaterminal.domain;

import android.annotation.SuppressLint;

import java.util.UUID;

import androidx.annotation.NonNull;

public class Voucher {

    private UUID uuid;
    private String productCode;
    private Boolean used;
    private String customerUUID;

    protected Voucher() {}

    public Voucher(UUID uuid, String productCode, Boolean used, String customerUUID) {
        this.uuid = uuid;
        this.productCode = productCode;
        this.used = used;
        this.customerUUID = customerUUID;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(String customerUUID) {
        this.customerUUID = customerUUID;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public String toString() {
        return String.format("Voucher[id=%s, productcode='%s', used='%b', customeruuid='%s']", uuid, productCode, used, customerUUID);
    }
}