package pt.up.fe.tiago.cafeteriaterminal;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pt.up.fe.tiago.cafeteriaterminal.initializer.CafeteriaTerminalApplication;
import pt.up.fe.tiago.cafeteriaterminal.repository.CafeteriaRepository;
import pt.up.fe.tiago.cafeteriaterminal.utils.Utils;
import pt.up.fe.tiago.cafeteriaterminal.utils.adapter.OrderResultAdapter;
import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CafeteriaOrderResultDTO;
import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CreateOrderDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {
    private static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private OrderResultAdapter orderResultAdapter;
    private ConstraintLayout totalContainer;
    private TextView totalPrice;
    private RecyclerView responseRecyclerView;
    private LinearLayout vouchersApplication;
    private ImageView voucherOneImage, voucherTwoImage;
    private TextView voucherOneTitle, voucherTwoTitle, orderNumber;

    @Inject
    Gson gson;

    @Inject
    CafeteriaRepository cafeteriaRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((CafeteriaTerminalApplication) this.getApplication()).getApplicationComponent().inject(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        FloatingActionButton QRCodeButton = findViewById(R.id.qr_code);
        vouchersApplication = findViewById(R.id.vouchers_application);
        voucherOneImage = findViewById(R.id.voucher_image_one);
        voucherOneTitle = findViewById(R.id.voucher_title_one);
        voucherTwoImage = findViewById(R.id.voucher_image_two);
        voucherTwoTitle = findViewById(R.id.voucher_title_two);
        responseRecyclerView = findViewById(R.id.response);
        totalContainer = findViewById(R.id.total_container);
        totalPrice = findViewById(R.id.total_price);
        orderNumber = findViewById(R.id.order_number);
        orderResultAdapter = new OrderResultAdapter(new ArrayList<>(), new ArrayList<>());
        responseRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        responseRecyclerView.setAdapter(orderResultAdapter);
        ViewCompat.setNestedScrollingEnabled(responseRecyclerView, false);
        QRCodeButton.setOnClickListener(v -> scan());
    }

    public void scan() {

        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE","QR_CODE_MODE" );
            startActivityForResult(intent, 0);
        }
        catch (ActivityNotFoundException e) {
            showDialog(this).show();
        }
    }

    private static AlertDialog showDialog(final MainActivity act) {

        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle("No Scanner Found");
        downloadDialog.setMessage("Download a scanner code activity?");
        downloadDialog.setPositiveButton("Yes", (dialogInterface, i) -> {
            Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            act.startActivity(intent);
        });
        downloadDialog.setNegativeButton("No", null);
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");

                CreateOrderDTO createOrderDTO = gson.fromJson(contents, CreateOrderDTO.class);
                cafeteriaRepository.processOrderInvoice(createOrderDTO).enqueue(new Callback<CafeteriaOrderResultDTO>() {
                    @Override
                    public void onResponse(Call<CafeteriaOrderResultDTO> call, Response<CafeteriaOrderResultDTO> response) {
                        if(response.body() != null) {

                            orderResultAdapter.removeData();
                            CafeteriaOrderResultDTO cafeteriaOrderResultDTO = response.body();

                            if(cafeteriaOrderResultDTO.getVouchers().size() >= 1)
                                Utils.fillVoucher(cafeteriaOrderResultDTO.getVouchers().get(0).getProductCode(), voucherOneImage, voucherOneTitle);
                            else {
                                voucherOneImage.setImageResource(R.drawable.ic_discount_voucher_100dp);
                                voucherOneTitle.setText(String.valueOf("No voucher applied."));
                            }

                            if(cafeteriaOrderResultDTO.getVouchers().size() >= 2)
                                Utils.fillVoucher(cafeteriaOrderResultDTO.getVouchers().get(1).getProductCode(), voucherTwoImage, voucherTwoTitle);
                            else {
                                voucherTwoImage.setImageResource(R.drawable.ic_discount_voucher_100dp);
                                voucherTwoTitle.setText(String.valueOf("No voucher applied."));
                            }

                            vouchersApplication.setVisibility(View.VISIBLE);
                            orderResultAdapter.addOrderInformation(cafeteriaOrderResultDTO.getProducts(), cafeteriaOrderResultDTO.getNumberOfProducts());
                            responseRecyclerView.setVisibility(View.VISIBLE);
                            totalPrice.setText(String.valueOf(String.format(Locale.getDefault(), "%.2f", cafeteriaOrderResultDTO.getTotalPrice()) + "$"));
                            orderNumber.setText(String.valueOf("Order: #" + cafeteriaOrderResultDTO.getInvoiceID()));
                            totalContainer.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<CafeteriaOrderResultDTO> call, Throwable t) {
                        System.out.println("ERROR: " + t.getMessage());
                    }
                });
            }
        }
    }
}
