package pt.up.fe.tiago.cafeteriaterminal.utils;

import android.widget.ImageView;
import android.widget.TextView;

import pt.up.fe.tiago.cafeteriaterminal.R;

public class Utils {

    public static void fillVoucher(String productCode, ImageView imageView, TextView textView) {
        if(productCode.toLowerCase().contains(Constants.POPCORN.toLowerCase())) {
            imageView.setImageResource(R.drawable.ic_popcorn);
            textView.setText(String.valueOf("Free"));
        } else if(productCode.toLowerCase().contains(Constants.COFFEE.toLowerCase())) {
            imageView.setImageResource(R.drawable.ic_coffee_for_voucher);
            textView.setText(String.valueOf("Free"));
        } else {
            imageView.setImageResource(R.drawable.ic_five_percent);
            textView.setText(String.valueOf("Discount"));
        }
    }
}
