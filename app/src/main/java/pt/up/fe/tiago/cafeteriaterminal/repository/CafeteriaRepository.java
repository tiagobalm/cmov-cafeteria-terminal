package pt.up.fe.tiago.cafeteriaterminal.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import pt.up.fe.tiago.cafeteriaterminal.webservice.Webservice;
import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CafeteriaOrderResultDTO;
import pt.up.fe.tiago.cafeteriaterminal.webservice.dto.CreateOrderDTO;
import retrofit2.Call;

@Singleton
public class CafeteriaRepository {

    private final Webservice webservice;

    @Inject
    CafeteriaRepository(Webservice webservice) { this.webservice = webservice; }

    public Call<CafeteriaOrderResultDTO> processOrderInvoice(CreateOrderDTO createOrderDTO) { return webservice.processOrderInvoice(createOrderDTO); }

}